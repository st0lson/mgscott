# Michael Scott and Machine Learning

### Building and generating your own quotes

Install the requirements file and you'll be in business.

```
pip install -r requirements.txt
python lib/generate_quotes.py --output 'output_quotes.txt'
```

![screenshot](mgscott/static/images/mgscott_screenshot.png)