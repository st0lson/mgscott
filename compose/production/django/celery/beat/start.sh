#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A mgscott.taskapp beat -l INFO
