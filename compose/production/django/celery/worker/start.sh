#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A mgscott.taskapp worker -l INFO
