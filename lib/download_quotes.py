from bs4 import BeautifulSoup
import requests
import re

# Thanks officequotes!!
HOME_URL = "http://officequotes.net/"
OUT_FILE = "outputs/full_quotes.txt"

# Get page info
page = requests.get(HOME_URL)
content = page.content
soup = BeautifulSoup(content, "html5lib")

# Get individual episode URLs
urls = []
url_regex = re.compile("no[0-9]\-[0-2][0-9]")

for url in soup.find_all("a"):
    if url_regex.match(url.attrs['href']):
        urls.append(HOME_URL+url.attrs['href'])
        
# Download all episode content
quotes = []
quote_regex = re.compile("Michael\:")
for url in urls:
    soup = BeautifulSoup(requests.get(url).content, "html5lib")
    raw = soup.find_all("div", "quote")
    for raw_quote in raw:
        for i in range(0, len(raw_quote.contents)):
            if quote_regex.match(str(raw_quote.contents[i].string)):
                quotes.append(str(raw_quote.contents[i+1].string))
                
# Write to file
with open(OUT_FILE, 'w') as f:
    for quote in quotes:
        f.write("%s\n" % quote.strip())

