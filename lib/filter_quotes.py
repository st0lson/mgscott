import re

OUT_FILE = "outputs/filter_quotes.txt"
IN_FILE = "outputs/full_quotes.txt"
FILTER_LENGTH = 10

# Quote class to better parse original quotes
class Quote:
    def __init__(self, quote_text):
        self.quote_text = quote_text

    def getText(self):
        while ('[' in self.quote_text):
            print(self.quote_text)
            self.quote_text = re.sub("[\(\[].*?[\)\]]", "", self.quote_text)
        self.quote_text = ' '.join(self.quote_text.split())
        return self.quote_text

# Get all quotes
raw_quotes = []
with open(IN_FILE, 'r') as f:
    for line in f:
        raw_quotes.append(Quote(line))

# Filter each quote and return new item
clean_quotes = []
for q in raw_quotes:
    new_quote = q.getText()
    if len(new_quote.split()) >= FILTER_LENGTH:
        clean_quotes.append(new_quote)

# Write to file
with open(OUT_FILE, 'w') as f:
    for quote in clean_quotes:
        f.write("%s\n" % quote)

