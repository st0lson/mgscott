$('.form-group').removeClass('row');

var PICTURE_RANGE = [1, 10]
var QUOTE_RANGE = [0, 62] 
function randomIntFromInterval(min,max){return Math.floor(Math.random()*(max-min+1)+min);}
$(document).ready(function() {
	// Get michael pic path
	var michaelPicPath = $('#michaelPic').attr('src').split('/').slice(0, -1).join('/');
	// Remove loader
	$(window).on('load', function() {
		// Remove the background loader
    	$('img').each(function () {
        	$(this).css('background-image', 'none');
    	});

    	$("#quoteText").text(quotes[randomIntFromInterval(QUOTE_RANGE[0], QUOTE_RANGE[1])]);
	});
	// Change michael scott pic and quote
	var quoteButton = $('#newQuote').click(function() {
		// Just to make sure it's not the same
		var oldQuote = $('#quoteText').text();
		var newQuote = quotes[randomIntFromInterval(QUOTE_RANGE[0], QUOTE_RANGE[1])];
		while (oldQuote === newQuote) {
			var newQuote = quotes[randomIntFromInterval(QUOTE_RANGE[0], QUOTE_RANGE[1])];
		}
		var oldPic = $('#michaelPic').attr('src');
		var newPic = michaelPicPath+"/mscott"+randomIntFromInterval(PICTURE_RANGE[0], PICTURE_RANGE[1]).toString()+".jpeg";
		while (oldQuote === newQuote) {
			var newPic = michaelPicPath+"/mscott"+randomIntFromInterval(PICTURE_RANGE[0], PICTURE_RANGE[1]).toString()+".jpeg";
		}
		$("#quoteText").text(newQuote);
		$('#michaelPic').attr('src', newPic);
	});

});